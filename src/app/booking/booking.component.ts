import { Component, OnInit,ViewChild } from '@angular/core';
import { ListChairComponent } from './listchair.component';

@Component({
  selector: 'app-booking',
  template: `
      <div class="container" style="max-width:850px">
          <h3 class="text-center mt-4 text-warning">Đặt vé xe bus hãng cybersoft</h3>
          <div class="row">
            <div class="col-md-6">
                <app-listchair #listchair ></app-listchair>
            </div>
            <div class="col-md-6">
                <app-listbooking (handleDelete)="handleDelete($event)" [booking]="booking"></app-listbooking>
            </div>
          </div>

      </div>
  `
})

export class BookingComponent implements OnInit {
  constructor() { }
  @ViewChild("listchair") tagListChair!:ListChairComponent;
  booking:chair[] = [];

  ngDoCheck(){
    if(this.tagListChair) {this.booking = this.tagListChair.listBooking};
    console.log(this.tagListChair ? this.tagListChair.listBooking : "");
  }
  handleDelete(id:number){
      const index = this.booking.findIndex((item)=>{
        return item.soGhe === id;
      })
      this.booking.splice(index,1);
      this.tagListChair.listBooking = this.booking;
  }
  ngOnInit() {

  }


}

interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}
