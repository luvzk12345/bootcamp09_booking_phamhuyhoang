import { Component, OnInit } from '@angular/core';

let arrChair:chair[] = [];
for (let index = 1; index <= 36; index++) {
  let trangThai = false;
  if(index === 11 || index === 30){
    trangThai = true;
  }
  arrChair.push({
    soGhe:index,
    tenGhe:`Ghế ${index}`,
    gia:100,
    trangThai,
  })
}
@Component({
  selector: 'app-listchair',
  template: `
    <div class="content">
        <button disabled class="btn btn-secondary w-100 mb-3">Danh sách ghế</button>
        <div class="row">
          <div class="col-md-3" *ngFor="let item of listChair">
              <app-chair [listBooking]="listBooking" [chairItem]="item" (handleBooking)="handleBook($event)"></app-chair>
          </div>
        </div>
    </div>
  `
})

export class ListChairComponent implements OnInit {

  listChair:chair[] = [...arrChair];
  listBooking:chair[] = [];

  constructor() { }

  ngOnInit() { }

  handleBook(item:chair){
      const index = this.listBooking.findIndex((chair:chair)=>{
          return chair.soGhe === item.soGhe;
      })
      if(index === -1){
          this.listBooking.push(item);
      }else{
         this.listBooking.splice(index,1);
      }
  }


}

interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}

