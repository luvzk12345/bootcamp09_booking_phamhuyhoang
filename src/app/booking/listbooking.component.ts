import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-listbooking',
  template: `
      <div>
          <h4 class="text-warning text-center">Danh sách ghế đã đặt ({{countBooking}})</h4>
          <div class="row">
              <div class="col-12" *ngFor="let item of booking">
                  <p>Ghế: {{item.tenGhe}} - $\{{item.gia}} <span (click)="handleRemove(item.soGhe)" class="text-danger" style="cursor: pointer;">Xóa</span></p>
              </div>
          </div>
      </div>
  `
})

export class ListBookingComponent implements OnInit {
  constructor() { }
  @Input() booking!:chair[];
  @Output() handleDelete = new EventEmitter<number>();
  ngOnInit() { }
  handleRemove(id:number){
      this.handleDelete.emit(id);
  }
  countBooking:number = 0;
  ngDoCheck(){
      this.countBooking = this.booking.reduce((total)=>{
          return total += 1;
      },0)
  }
}
interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}
